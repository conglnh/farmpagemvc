﻿using System;
using System.Linq;

namespace Domain.DefaultData
{
    public class DefaultDataPack
    {
        public virtual void Import(FarmContext context)
        {
            ScanAllDataPackItem(context);
        }

        private void ScanAllDataPackItem(FarmContext context)
        {
            foreach (Type mytype in System.Reflection.Assembly.GetExecutingAssembly().GetTypes()
                .Where(mytype => mytype.GetInterfaces().Contains(typeof(IMasterDataItem))))
            {
                dynamic instance = Activator.CreateInstance(mytype);
                instance.ImportData(context);
            }
        }
    }
}
