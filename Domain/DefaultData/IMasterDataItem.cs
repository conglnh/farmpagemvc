﻿using System.Data.Entity;

namespace Domain.DefaultData
{
    public interface IMasterDataItem
    {
        void ImportData(DbContext context);
    }
}
