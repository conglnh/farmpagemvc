﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity
{
    public class BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime? CreateDate{ get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? CreateById { get; set; }
        public int? UpdateById { get; set; }
    }
}
