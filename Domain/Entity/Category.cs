﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entity
{
    public class Category : BaseEntity
    {
        [Required]
        public string Label { get; set; }
        [Required]
        public string Description { get; set; }
        public virtual ICollection<Product> Products { get; set; } 
    }
}
