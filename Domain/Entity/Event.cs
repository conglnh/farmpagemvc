﻿using System.Collections.Generic;

namespace Domain.Entity
{
    public class Event : BaseEntity
    {
        public string Label { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public virtual ICollection<ImageContain> ImageContains{ get; set; }
    }
}