﻿namespace Domain.Entity
{
    public class EventTesty : BaseEntity
    {
        public string Label { get; set; }
        public string Description { get; set; }
    }
}