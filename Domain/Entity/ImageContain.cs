﻿using System.Collections.Generic;
using System.Web;

namespace Domain.Entity
{
    public class ImageContain : BaseEntity
    {
        public byte[] Image { get; set; }
        public string ImageName { get; set; }
        public string ImageExtension { get; set; }
        public int? EventId { get ; set; }
        public int? ProductId { get ; set; }
        public Product Product { get ; set; }
        public virtual Event Event { get; set; }
    }
}
