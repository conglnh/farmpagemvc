﻿using System.Collections.Generic;

namespace Domain.Entity
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public int Quatity { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public virtual ICollection<ImageContain> ImageContains { get; set; }
    }
}
