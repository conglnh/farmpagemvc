﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Domain.DefaultData;
using Domain.Entity;
using Domain.Entity.Mapping;

namespace Domain
{
    public class FarmContext : DbContext
    {
        public FarmContext()
            : base("name=FarmPage2")
        {
            Console.WriteLine("Initializing Database......success");
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventTesty> EventTestys { get; set; }
        public DbSet<ImageContain> ImageContains { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            /*Database.SetInitializer(new MyDbContextInitializer());*/
            modelBuilder.Configurations.Add(new ProductMapping());
            modelBuilder.Configurations.Add(new CategoryMapping());
            modelBuilder.Configurations.Add(new EventMapping());
            modelBuilder.Configurations.Add(new EventTestyMapping());
            modelBuilder.Configurations.Add(new ImageContainMapping());
        }

        public new class Configuration : DbMigrationsConfiguration<FarmContext>
        {
            public Configuration()
            {
                AutomaticMigrationsEnabled = true;
                AutomaticMigrationDataLossAllowed = true;
            }

            protected override void Seed(FarmContext context)
            {
                new DefaultDataPack().Import(context);
                context.SaveChanges();
            }
        }
    }
}
