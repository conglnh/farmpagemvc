﻿using System;

namespace Farm.AppService.DTO
{
    public class BaseDto
    {
        public int Id { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public Guid? CreateById { get; set; }
        public Guid? UpdateById { get; set; }
    }
}
