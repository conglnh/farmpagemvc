﻿using System.Collections.Generic;

namespace Farm.AppService.DTO
{
    public class CategoryDto : BaseDto
    {
        public string Description { get; set; }
        public virtual ICollection<ProductDto> Products { get; set; } 
    }
}
