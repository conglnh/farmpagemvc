﻿using System.Collections.Generic;
using System.Web;

namespace Farm.AppService.DTO
{
    public class ProductDto
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public string ImgUrl { get; set; }
        public string Description { get; set; }
        public int Quatity { get; set; }
        public IEnumerable<HttpPostedFileBase> files { get; set; }
        public CategoryDto Category { get; set; }
    }
}
