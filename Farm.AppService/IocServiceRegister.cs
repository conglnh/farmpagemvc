﻿using System.Reflection;
using Autofac;
using Farm.AppService.Services;
using Farm.Repository;
using Farm.Repository.IService;
using Module = Autofac.Module;

namespace Farm.AppService
{
    public class IocServiceRegister : Module
    {
        protected override void Load(ContainerBuilder builder)
        {          
            builder.RegisterType<ProductService>().As<IProductService>();
            builder.RegisterType<CategoryService>().As<ICategoryService>();
            builder.RegisterType<EventService>().As<IEventServices>();
            builder.RegisterModule(new IocRegister());
            base.Load(builder);
        }
    }
}
