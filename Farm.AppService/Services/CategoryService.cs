﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Domain;
using Domain.Entity;
using Farm.Repository.IService;
using Farm.Repository.Repository;

namespace Farm.AppService.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IRepository<Category> _repository;

        public CategoryService(ICategoryRepository categoryRepository, IRepository<Category> repository)
        {
            _categoryRepository = categoryRepository;
            _repository = repository;
        }

        public void Add(Category category)
        {
            _categoryRepository.Add(category);
        }

        public void Delete(int categoryId)
        {
            _categoryRepository.Delete(categoryId);
        }

        public IQueryable<Category> GetAll()
        {
            return _categoryRepository.GetAll();
        }

        public void Edit(int pk, string name, string newValue)
        {
            UpdateCampaign(pk, name, newValue);
            var entitiy = _repository.GetById(pk);
           /* FarmContext db = new FarmContext();
            entitiy.Label = newValue;
            _repository.Edit(entitiy);*/
        }

        /* public void UpdateCampaign<T>(List<T> data,int id, string key, string value) where T : BaseEntity
         {
             using (var context = new FarmContext())
             {
                 var camp = data.Single(e => e.Id == id);
                 SetProperty(camp, key, value);
                 context.SaveChanges();
             }
         }
         public void SetProperty(object source, string property, object value)
         {
             string[] bits = property.Split('.');
             for (int i = 0; i < bits.Length - 1; i++)
             {
                 PropertyInfo prop = source.GetType().GetProperty(bits[i]);
                 source = prop.GetValue(source, null);
             }
             PropertyInfo propertyToSet = null;
             if (source is IEnumerable)
             {
                 foreach (object o in (source as IEnumerable))
                 {
                     propertyToSet = o.GetType().GetProperty(bits[bits.Length - 1]);
                     propertyToSet.SetValue(o, value, null);
                     break;
                 }
             }
             else
             {
                 propertyToSet = source.GetType().GetProperty(bits[bits.Length - 1]);
                 propertyToSet.SetValue(source, value, null);
             }
         }*/

        public void UpdateCampaign(int id, string key, string value)
        {
            using (var context = new FarmContext())
            {
                var camp = context.Categories.Single(e => e.Id == id);
                SetProperty(camp, key, value);
                context.SaveChanges();
            }
        }

        public void SetProperty(object source, string property, object value)
        {
            string[] bits = property.Split('.');
            for (int i = 0; i < bits.Length - 1; i++)
            {
                PropertyInfo prop = source.GetType().GetProperty(bits[i]);
                source = prop.GetValue(source, null);
            }
            PropertyInfo propertyToSet = null;
            if (source is IEnumerable)
            {
                foreach (object o in (source as IEnumerable))
                {
                    propertyToSet = o.GetType().GetProperty(bits[bits.Length - 1]);
                    propertyToSet.SetValue(o, value, null);
                    break;
                }
            }
            else
            {
                propertyToSet = source.GetType().GetProperty(bits[bits.Length - 1]);
                propertyToSet.SetValue(source, value, null);
            }
        }
    }
}