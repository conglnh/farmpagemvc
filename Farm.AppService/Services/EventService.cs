﻿using System.Collections;
using System.Linq;
using System.Reflection;
using Domain;
using Domain.Entity;
using Farm.Repository.IService;
using Farm.Repository.Repository;

namespace Farm.AppService.Services
{
    public class EventService : IEventServices
    {
        private readonly IRepository<Event> _repository;

        public EventService(IRepository<Event> repository)
        {
            _repository = repository;
        }
        public void Add(Event @event)
        {
            _repository.Add(@event);
        }

        public void Delete(int eventId)
        {
            _repository.Delete(eventId);
        }

        public IQueryable<Event> GetAll()
        {
            return _repository.GetAll();
        }

        public void Edit(int pk, string name, string newValue)
        {
            UpdateCampaign(pk, name, newValue);
            var entitiy = _repository.GetById(pk);
        }

        public int Update(Event @event)
        {
            return _repository.Edit(@event);
        }

        public void UpdateCampaign(int id, string key, string value)
        {
            using (var context = new FarmContext())
            {
                var camp = context.Events.Single(e => e.Id == id);
                SetProperty(camp, key, value);
                context.SaveChanges();
            }
        }

        public void SetProperty(object source, string property, object value)
        {
            string[] bits = property.Split('.');
            for (int i = 0; i < bits.Length - 1; i++)
            {
                PropertyInfo prop = source.GetType().GetProperty(bits[i]);
                source = prop.GetValue(source, null);
            }
            PropertyInfo propertyToSet = null;
            if (source is IEnumerable)
            {
                foreach (object o in (source as IEnumerable))
                {
                    propertyToSet = o.GetType().GetProperty(bits[bits.Length - 1]);
                    propertyToSet.SetValue(o, value, null);
                    break;
                }
            }
            else
            {
                propertyToSet = source.GetType().GetProperty(bits[bits.Length - 1]);
                propertyToSet.SetValue(source, value, null);
            }
        }
    }
}