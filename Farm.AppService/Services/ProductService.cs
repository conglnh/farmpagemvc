﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Domain;
using Domain.Entity;
using Farm.Repository.IService;
using Farm.Repository.Repository;

namespace Farm.AppService.Services
{
    public class ProductService : IProductService
    {
        private IProductRepository _productRepository;

        public ProductService(IProductRepository repository)
        {
            _productRepository = repository;
        }

        public void Add(Product product)
        {
            _productRepository.Add(product);
        }

        public IQueryable<Product> GetAll()
        {
            return _productRepository.GetAll();
        }

        public void Delete(int productId)
        {
            var db = new FarmContext();
            var imageContain = db.ImageContains.Where(img => img.ProductId == productId);
            db.ImageContains.RemoveRange(imageContain);
            db.SaveChanges();
            _productRepository.Delete(productId);
        }

        public void Edit(int pk, string name, string value)
        {
            UpdateCampaign(pk, name, value);
        }

        public Product GetById(int productId)
        {
            return _productRepository.GetById(productId);
        }

        public void UpdateCampaign(int id, string key, string value)
        {
            using (var context = new FarmContext())
            {
                var camp = context.Products.Single(e => e.Id == id);
                SetProperty(camp, key, value);
                context.SaveChanges();
            }
        }

        public void SetProperty(object source, string property, object value)
        {
            double price = 0;
            var result = double.TryParse(value.ToString(), out price);
            if (result)
            {
                value = price;
            }
            string[] bits = property.Split('.');
            for (int i = 0; i < bits.Length - 1; i++)
            {
                PropertyInfo prop = source.GetType().GetProperty(bits[i]);
                source = prop.GetValue(source, null);
            }
            PropertyInfo propertyToSet = null;
            if (source is IEnumerable)
            {
                foreach (object o in (source as IEnumerable))
                {
                    propertyToSet = o.GetType().GetProperty(bits[bits.Length - 1]);
                    propertyToSet.SetValue(o, value, null);
                    break;
                }
            }
            else
            {
                propertyToSet = source.GetType().GetProperty(bits[bits.Length - 1]);
                propertyToSet.SetValue(source, value, null);
            }
        }
    }
}