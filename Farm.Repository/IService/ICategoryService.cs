﻿using System.Linq;
using Domain.Entity;

namespace Farm.Repository.IService
{
    public interface ICategoryService
    {
        void Add(Category category);
        void Delete(int categoryId);
        IQueryable<Category> GetAll();
        void Edit(int pk, string name,string newValue);
    }
}