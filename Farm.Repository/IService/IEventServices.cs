﻿using System.Linq;
using Domain.Entity;

namespace Farm.Repository.IService
{
    public interface IEventServices
    {
        void Add(Event @event);
        void Delete(int @eventId);
        IQueryable<Event> GetAll();
        void Edit(int pk, string name, string newValue);
        int Update(Event @event);
    }
}