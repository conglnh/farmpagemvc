﻿using System.Linq;
using Domain.Entity;

namespace Farm.Repository.IService
{
    public interface IProductService
    {
        void Add(Product product);
        IQueryable<Product> GetAll();
        void Delete(int productId);
        void Edit(int pk, string name, string value);
        Product GetById(int productId);
    }
}