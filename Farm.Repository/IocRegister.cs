﻿using System.Reflection;
using Autofac;
using Domain;
using Domain.Entity;
using Farm.Repository.Repository;
using Farm.Repository.Repository.Implement;
using Module = Autofac.Module;

namespace Farm.Repository
{
    public class IocRegister : Module
    {
        private string connStr;
        /* public IocRegister(string connString)
         {
             this.connStr = connString;
         }*/

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.Load("Farm.Repository"))

                     .Where(t => t.Name.EndsWith("Repository"))

                     .AsImplementedInterfaces()

                     .InstancePerLifetimeScope();
            //builder.RegisterModule(new Farm.Repository.IocRegister());
            builder.RegisterType<Repository.Implement.Repository<FarmContext, Category>>().As<IRepository<Category>>();
            builder.RegisterType<Repository.Implement.Repository<FarmContext, Event>>().As<IRepository<Event>>();

            base.Load(builder);
        }
    }
}
