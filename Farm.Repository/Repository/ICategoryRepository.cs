﻿using Domain.Entity;

namespace Farm.Repository.Repository
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}