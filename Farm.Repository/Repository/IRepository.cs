﻿using System.Collections.Generic;
using System.Linq;
using Domain.Entity;

namespace Farm.Repository.Repository
{
    public interface IRepository<T> where T : BaseEntity
    {
        void Add(T entity);
        IQueryable<T> GetAll();
        void Delete(int id);
        T GetById(int id);
        int Edit(T t);
        
    }
}
