﻿using Domain.Entity;

namespace Farm.Repository.Repository
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}