﻿using System;
using System.Data.Entity;
using System.Linq;
using Domain.Entity;

namespace Farm.Repository.Repository.Implement
{
    public class Repository<TC, T> : IRepository<T>
        where T : BaseEntity
        where TC : DbContext, new()
    {

        private readonly TC _entities = new TC();
        public virtual IQueryable<T> GetAll()
        {

            IQueryable<T> query = _entities.Set<T>();
            return query;
        }

        public IQueryable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {

            IQueryable<T> query = _entities.Set<T>().Where(predicate);
            return query;
        }

        public virtual void Add(T entity)
        {
            _entities.Set<T>().Add(entity);
            Save();
        }

        public virtual void Delete(int id)
        {
            var entity = _entities.Set<T>().FirstOrDefault(p => p.Id == id);
            _entities.Set<T>().Remove(entity);
            Save();
        }

        public T GetById(int id)
        {
            var entity = _entities.Set<T>().FirstOrDefault(p => p.Id == id);
            return entity;
        }

        int IRepository<T>.Edit(T t)
        {
            _entities.Entry(t).State = EntityState.Modified;
            _entities.SaveChanges();
            return t.Id;
        }

        public virtual void Edit(T entity)
        {
            _entities.Entry(entity).State = EntityState.Modified;
            Save();
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }
    }
}
