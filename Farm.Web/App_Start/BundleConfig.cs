﻿using System.Web;
using System.Web.Optimization;

namespace Farm.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/js/HomePage").Include(
                      "~/js/bootstrap.min.js",
                      "~/js/counterup.min.js",
                      "~/js/easing.js",
                      "~/js/jquery.flexslider.js",
                      "~/js/minicart.min.js",
                      "~/js/move-top.js",
                      "~/js/waypoints.min.js"));

            bundles.Add(new ScriptBundle("~/js/Admin").Include(
                      "~/Content/Admin/AdminJs/Chart.js",
                      "~/Content/Admin/AdminJs/wow.min.js",
              
                      "~/Content/Admin/AdminJs/classie.js",
                      "~/Content/Admin/AdminJs/uisearch.js",
                      "~/Content/Admin/AdminJs/jquery.nicescroll.js",
                  /*    "~/Content/Admin/AdminJs/scripts.js",
                      "~/Content/Admin/AdminJs/bootstrap.min.js",*/
                      "~/Content/Admin/AdminJs/skycons.js"));

            bundles.Add(new StyleBundle("~/Content/AdminCss").Include(
                      "~/Content/Admin/bootstrap.min.css",
                      "~/Content/Admin/style.css",
                      "~/Content/Admin/font-awesome.css",
                      "~/Content/Admin/icon-font.min.css",
                      "~/Content/Admin/animate.css",
                      "~/Content/font-awesome.css"));
        }
    }
}
