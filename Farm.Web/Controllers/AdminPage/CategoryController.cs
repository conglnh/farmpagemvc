﻿using System.Linq;
using System.Web.Mvc;
using Domain.Entity;
using Farm.Repository.IService;

namespace Farm.Web.Controllers.AdminPage
{
    public class CategoryController : Controller
    {
        private ICategoryService _iCategoryService;

        public CategoryController(ICategoryService iCategoryService)
        {
            _iCategoryService = iCategoryService;
        }

        // GET: Category
        public ActionResult Index()
        {
            var result = _iCategoryService.GetAll();
            return View(result.ToList());
        }

        [HttpGet]
        public ActionResult AddCategory()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddCategory(Category category)
        {
            if(!ModelState.IsValid) return View();
            _iCategoryService.Add(category);
            return RedirectToAction("Index");
        }
    }
}