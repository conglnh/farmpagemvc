﻿using System.Web.Mvc;
using Farm.Repository.IService;

namespace Farm.Web.Controllers
{
    public class HomeController : Controller
    {
        private IProductService _iProductService;

        public HomeController(IProductService iProductService)
        {
            _iProductService = iProductService;
        }
        public ActionResult Index()
        {
            var products = _iProductService.GetAll();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}