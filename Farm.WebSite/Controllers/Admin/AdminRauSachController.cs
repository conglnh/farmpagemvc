﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain;
using Domain.Entity;
using Farm.Repository.IService;
using Farm.WebSite.ViewModel;

namespace Farm.WebSite.Controllers.Admin
{
    public class AdminRauSachController : Controller
    {
        private ICategoryService _iCategoryService;
        private IEventServices _iEventService;
        private IProductService _iProductService;

        public AdminRauSachController(ICategoryService iCategoryService, IEventServices iEventService, IProductService iProductService)
        {
            _iCategoryService = iCategoryService;
            _iEventService = iEventService;
            _iProductService = iProductService;
        }
        // GET: AdminRauSach
        public ActionResult Index()
        {

            return View();
        }
        #region Categories
        public ActionResult Categories()
        {
            var result = _iCategoryService.GetAll();
            return View(result.ToList());
        }

        public ActionResult GetCategories()
        {
            try
            {
                var result = _iCategoryService.GetAll();
                return View(result.ToList());
            }
            catch (System.Exception ex)
            {
                return Json(ex);
            }

        }

        public ActionResult DeleteCategory(int id)
        {
            _iCategoryService.Delete(id);
            return Json(new { data = "success", message = "Xóa sản phẩm thành công" });
        }

        public ActionResult EditCategory(int pk, string name, string value)
        {
            _iCategoryService.Edit(pk, name, value);
            return Json(new { data = "success", message = "Xóa sản phẩm thành công" });
        }


        public ActionResult AddCategory()
        {
            return View("_AddCategories");
        }


        [HttpPost]
        public ActionResult AddCategory(Category category)
        {
            _iCategoryService.Add(category);
            return Json(new
            {
                data = "success",
                message = "Thêm Thành Công"
            }, JsonRequestBehavior.AllowGet);
            /*    return Json(new { data = "success", message = "Thêm Thành Công" });*/
        }
        #endregion
        #region Event

        public ActionResult Events()
        {
            return View();
        }

        public ActionResult GetEvent()
        {
            var result = _iEventService.GetAll();
            return View(result.ToList());
        }

        public ActionResult AddEvent()
        {
            return PartialView("_AddCEvents");
        }

        [HttpPost]
        public ActionResult AddEvent(EventViewModel eventViewModel, List<HttpPostedFileBase> images)
        {
            FarmContext db = new FarmContext();
            var @event = new Event()
            {
                Label = eventViewModel.Label,
                Description = eventViewModel.Description,
                Content = eventViewModel.Content,
                CreateDate = DateTime.Now
            };
            _iEventService.Add(@event);
            //todo:Refactor to service Add Image and design agian for table image (Too many Id and missing ext(.png.jpg),Name)
            try
            {
                foreach (HttpPostedFileBase item in eventViewModel.files)
                {
                    var fileData = new MemoryStream();
                    item.InputStream.CopyTo(fileData);

                    var imageEvent = new ImageContain()
                    {
                        EventId = @event.Id,
                        CreateDate = DateTime.Now,
                        Image = fileData.ToArray(),
                    };
                    db.ImageContains.Add(imageEvent);
                    db.SaveChanges();
                }
                return Json(new { data = "success", message = "Thêm Thành Công" });
            }
            catch (Exception)
            {
                return Json(new { data = "error", message = "Hình lớn hơn 2MB" });
            }

        }

        public ActionResult EditEvent(int pk, string name, string value)
        {
            _iEventService.Edit(pk, name, value);
            return Json(new { data = "success", message = "Sửa sản phẩm thành công" });
        }

        public ActionResult DeleteEvent(int id)
        {
            _iEventService.Delete(id);
            return Json(new { data = "success", message = "Xóa sản phẩm thành công" });
        }
        #endregion
        #region Product

        public ActionResult Product()
        {
            return View();
        }

        public ActionResult GetProduct()
        {
            try
            {
                var result = _iProductService.GetAll();
                return View(result.ToList());
            }
            catch (System.Exception ex)
            {
                return Json(ex);
            }
        }

        public ActionResult AddProduct()
        {
            var category = _iCategoryService.GetAll();
            var productViewModel = new ProductViewModel();
            productViewModel.Category = category.ToList().Select(f => new SelectListItem()
            {
                Text = f.Label,
                Value = f.Id.ToString()
            }).ToList();
            return PartialView("_AddProduct", productViewModel);
        }

        [HttpPost]
        public ActionResult AddProduct(ProductViewModel productViewModel, HttpPostedFileBase files)
        {
            FarmContext db = new FarmContext();
            var product = new Product()
            {
                Description = productViewModel.Description,
                CreateDate = DateTime.Now,
                CategoryId = productViewModel.CategoryId,
                Name = productViewModel.Name,
                Price = productViewModel.Price,
                Quatity = productViewModel.Quatity,
            };
            _iProductService.Add(product);

            //todo:Refactor to service Add Image and design agian for table image (Too many Id and missing ext(.png.jpg),Name)
            try
            {
                foreach (HttpPostedFileBase item in productViewModel.files)
                {
                    var fileData = new MemoryStream();
                    item.InputStream.CopyTo(fileData);

                    var imageEvent = new ImageContain()
                    {
                        ProductId = product.Id,
                        CreateDate = DateTime.Now,
                        Image = fileData.ToArray(),
                    };
                    db.ImageContains.Add(imageEvent);
                    db.SaveChanges();
                }
                return Json(new { data = "success", message = "Thêm Thành Công" });
            }
            catch (Exception)
            {
                return Json(new { data = "error", message = "Hình lớn hơn 2MB" });
            }
        }

        public ActionResult EditProduct(int pk, string name, string value)
        {
            _iProductService.Edit(pk, name, value);
            return Json(new { data = "success", message = "Sửa sản phẩm thành công" });
        }

        public ActionResult DeleteProduct(int id)
        {
            _iProductService.Delete(id);
            return Json(new { data = "success", message = "Xóa Thành Công" });
        }

        public ActionResult ProductDetail(int id)
        {
            _iProductService.GetById(id);
            return View();
        }
        #endregion
    }
}