﻿using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Facebook;
using Farm.Repository.IService;
using Farm.WebSite.ViewModel;

namespace Farm.WebSite.Controllers
{
    public class HomeController : Controller
    {

        private Uri RedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = Url.Action("FaceBookCallBack","Home");
                return uriBuilder.Uri;
            }
        }
        private ICategoryService _iCategoryService;
        private IEventServices _iEventService;

        public HomeController(ICategoryService iCategoryService, IEventServices iEventService)
        {
            _iCategoryService = iCategoryService;
            _iEventService = iEventService;
        }
        public ActionResult Index()
        {
            var @event = _iEventService.GetAll().OrderByDescending(evt => evt.CreateDate).Take(3).ToList();
            var model = new IndexHomeViewModel()
            {
                Events = @event,
            };

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Products()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult HomeEvent(int eventId)
        {
            var @event = _iEventService.GetAll().FirstOrDefault(evt => evt.Id == eventId);

            return View(@event);
        }

        public ActionResult LoginFaceBook()
        {
            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = ConfigurationManager.AppSettings["FaceBookId"],
                client_secret = ConfigurationManager.AppSettings["FacebooSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                respone_type = "code",
                scope = "email",
            });

            return Redirect(loginUrl.AbsoluteUri);
        }

        public ActionResult FaceBookCallBack(string code)
        {
            //refer link : https://www.youtube.com/watch?v=N6ROz-F7u4g
            //todo : Refactor to service and add User to Database for authenticate or authorization
            var fb = new FacebookClient();
            dynamic result = fb.Post("oauth/access_token", new
            {
                client_id = ConfigurationManager.AppSettings["FaceBookId"],
                client_secret = ConfigurationManager.AppSettings["FacebooSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                code = code,
            });

            var accessToken = result.access_token;

            if (!string.IsNullOrEmpty(accessToken))
            {
                fb.AccessToken = accessToken;
                dynamic me = fb.Get("me?fields=first_name,middle_name,last_name,id,email");
                string email = me.email;
                string username = me.email;
                string firstName = me.first_name;
                string lastName = me.last_name;
                string middle_name = me.middle_name;

                Session["email"] = firstName + " " + middle_name + " " + lastName;
            }
            return RedirectToAction("Index");
        }

        public ActionResult LogoutFaceBook()
        {
            Session["email"] = null;
            return RedirectToAction("Index");
        }
    }
}