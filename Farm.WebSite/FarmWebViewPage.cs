﻿using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;

namespace Farm.WebSite
{
    public class FarmWebViewPage<T> : WebViewPage<T>
    {

        public string ApplicationName => _applicationName;
        private static readonly string _applicationName;
        static FarmWebViewPage()
        {
            _applicationName = ConfigurationManager.AppSettings["applicationName"];
        }

        public override void Execute()
        {
            ViewBag.Title = Title;
            ViewBag.Modal = Modal;
        }

        public FarmWebViewPage.ModalData Modal
        {
            get
            {
                if (!ViewData.ContainsKey("Modal"))
                    ViewData["Modal"] = new FarmWebViewPage.ModalData();
                return ViewData["Modal"] as FarmWebViewPage.ModalData;
            }
        }

        public string Title
        {
            get { return ViewBag.Title; }
            set { ViewBag.Title = value; }
        }
    }

    public class FarmWebViewPage : FarmWebViewPage<object>
    {
        public class ModalData
        {
            public static string Layout = @"~/Views/shared/_LayoutModal.cshtml";
            public string CancelText = "Cancel";
            public string CancelStyle = "";
            public bool ShowCancel = true;
            public string OkText = "OK";
            public string OkStyle = "btn-primary";

            public object HtmlAttributes;
            public bool Fluid = false;

            public string HttpMethod = "POST";
            public bool IsAjax = true;
            public bool Form = true;
            public string Url = null;
            public string Action = null;
            public string Controller = null;
            public string Values = null;
            public string OnComplete = null;


            /*public List<string> AutoLoad = new List<string>();*/
            public List<string> FadeOutElements = new List<string>();
            public List<string> HighlightElements = new List<string>();
        }
    }
}