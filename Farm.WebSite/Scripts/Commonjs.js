﻿function showAlert(message, type, closeDelay) {

    if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body").append($('<div id="alerts-container" style="z-index:9999;position: fixed;width: 50%; left: 75%; top: 10%;">'));
    }

    // default to alert-info; other options include success, warning, danger
    type = type || "info";

    // create the alert div
    var alert = $('<div class="alert alert-' + type + ' fade in">')
        .append(
            $('<button type="button" class="close" data-dismiss="alert">')
            .append("&times;")
        )
        .append(message);

    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);

    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay) {
        window.setTimeout(function () { alert.alert("close") }, closeDelay);
    }
    else {
        window.setTimeout(function () { alert.alert("close") }, 2000);
    }
}

function ShowSuccess(message) {
    showAlert(message, 'success', 2000);
}

function ShowError(message) {
    showAlert(message, 'error', 2000);
}

function ShowInfo(message) {
    showAlert(message, 'info', 2000);
}

function close_dialog() {
    $("#dialog-edit").dialog('close');
}

function Autoload(target) {
    var url = target.data('autoload');
    target.load(url);
}