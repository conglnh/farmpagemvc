﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Farm.WebSite.Startup))]
namespace Farm.WebSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
