﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Farm.WebSite.ViewModel
{
    public class EventViewModel
    {
        [Required]
        public string Label { get; set; }
        [Required]
        public string Description { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        public IEnumerable<HttpPostedFileBase> files { get; set; }
    }
}