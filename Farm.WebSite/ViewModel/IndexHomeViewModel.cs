﻿using System.Collections.Generic;
using Domain.Entity;

namespace Farm.WebSite.ViewModel
{
    public class IndexHomeViewModel
    {
        public List<Event> Events { get; set; }
    }
}