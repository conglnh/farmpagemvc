﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Farm.AppService.DTO;

namespace Farm.WebSite.ViewModel
{
    public class ProductViewModel
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public string ImgUrl { get; set; }
        public string Description { get; set; }
        public int Quatity { get; set; }
        public IEnumerable<HttpPostedFileBase> files { get; set; }
        public int CategoryId { get; set; }
        public List<SelectListItem> Category { get; set; }
    }
}