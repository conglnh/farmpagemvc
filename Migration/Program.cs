﻿using System.Data.Entity;
using Domain;
using Farm.AppService.Services;

namespace Migration
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var migrator = new MigrateDatabaseToLatestVersion<FarmContext, FarmContext.Configuration>();
            Database.SetInitializer(migrator);
            migrator.InitializeDatabase(new FarmContext());
        }
    }
}
